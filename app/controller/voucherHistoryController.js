// import thư viện mongose 
const mongoose = require("mongoose");
//improt VoucherHistoryModel 
const VoucherHistoryModel = require("../model/voucherHistoryModel");
// Tạo function createVoucherHistory
const createVoucherHistory =(req,res)=>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    // có thể tìm theo điều kiện thêm điều kiện tìm theo điều kiện theo hướng dẫn find(condition).functin().function().exc..
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.user)){
        return res.status(400).json({
            message: "Id user không hợp lệ"
        })
    };
    if(!mongoose.Types.ObjectId.isValid(body.voucher)){
        return res.status(400).json({
            message: "Id voucher không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        voucher: body.voucher
    }

    VoucherHistoryModel.create(newVoucherHistory,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                User: data
            })
        }
    }) 
}; 
// Tạo function getAllVoucherHistory
const getAllVoucherHistory = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    // let userCheck = req.query.userCheck;
    // let condition = {}
    // có thể tìm theo điều kiện thêm điều kiện tìm theo điều kiện theo hướng dẫn find(condition).functin().function().exc..
    //B2: validate dữ liệu
    // if(userCheck){
    //     condition.user = userCheck
    // }
    // //B3: Gọi model thực hiện các thao tác nghiệp vụ
    // VoucherHistoryModel.find(condition).exec((err,data)=>{
    //     if(err){
    //         return res.status(500).json({
    //              message: err.message
    //          })
    //      }
    //      else{
    //          return res.status(201).json({
    //              message:"Tải toàn bộ dữ liệu thành công",
    //              data: data
    //          })
    //      }
    // })
    let user = req.query.user
    let condition = {}
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: 'user không hợp lệ'
        })
    }
    if (user) {
        condition.user = user
    }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    VoucherHistoryModel.find(condition).populate("user").populate("voucher").exec((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 data: data
             })
         }
    })
};
// Tạo function getVoucherHistoryById 
const getVoucherHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId
    // có thể tìm theo điều kiện thêm điều kiện tìm theo điều kiện theo hướng dẫn find(condition).functin().function().exc..
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'Id không hợp lệ'
        })
      }
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    VoucherHistoryModel.findById(id)
    .populate("user")
    .populate("voucher")
    .exec((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message 
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải dữ liệu thành công thông qua Id",
                 data: data
             })
         }
    })
};
// Tạo function updateVoucherHistoryById
const updateVoucherHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.historyId;
    let body = req.body;
    // có thể tìm theo điều kiện thêm điều kiện tìm theo điều kiện theo hướng dẫn find(condition).functin().function().exc..
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'Id không hợp lệ'
        })
    };
    if(!mongoose.Types.ObjectId.isValid(body.user)){
        return res.status(400).json({
            message: "Id user không hợp lệ"
        })
    };
    if(!mongoose.Types.ObjectId.isValid(body.voucher)){
        return res.status(400).json({
            message: "Id voucher không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    let voucherHistoryUpdate = {
        user: body.user,
        voucher: body.voucher
      }
    VoucherHistoryModel.findByIdAndUpdate(id,voucherHistoryUpdate,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng  ID",
                 updateData: data
             })
         }
    })
};
// Tạo function deleteVoucherHistoryById
const deleteVoucherHistoryById = (req,res)=>{
    //B1: thu thập dữ liệu từ reqư
    let id = req.params.historyId;
    // có thể tìm theo điều kiện thêm điều kiện tìm theo điều kiện theo hướng dẫn find(condition).functin().function().exc..
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
          message: 'Id không hợp lệ'
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    VoucherHistoryModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Thành công xoá dữ liệu thông qua ID",
                 DeleteData: data
             })
         }
    })
}

//exprot controller
module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}

// import thư viện mongose 
const mogoose = require("mongoose");
//improt UserModel 
const userModel = require("../model/userModel");

//Tạo function createUser
const createUser = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let body = req.body
    console.log(body)
    //B2 Kiểm tra dữ liệu
    if(!body.username) {
        return res.status(400).json({
          message: 'dữ liệu UserName không đúng'
        })
      }
      if(!body.firstname) {
        return res.status(400).json({
          message: 'Dữ liệu first Name không đúng'
        })
      }
      if(!body.lastname) {
        return res.status(400).json({
          message: 'Dữ liệu last Name không đúng'
        })
      }
      // B3 Gọi model thực hiện các thao tác nghiệp vụ
      let newUser = {
        _id: mogoose.Types.ObjectId(),
        username: body.username,
        lastname: body.lastname,
        firstname: body.firstname
      }
      userModel.create(newUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: err.message
            })
        }
        else{
            return res.status(201).json({
                message:"Tạo mới thành công",
                User: data
            })
        }
      })
};
//Tạo function getAllUser
const getAllUser = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    //B2: validate dữ liệu
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải toàn bộ dữ liệu thành công",
                 User: data
             })
         }
    })
};
//Tạo function getUserById
const getUserById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id = req.params.userId
    //B2: validate dữ liệu
    if(!mogoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findById(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(201).json({
                 message:"Tải dữ liệu thành công thông qua Id",
                 User: data
             })
         }
    })
};
// Tạo function updateUserById
const updateUserById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id= req.params.userId;
    let body = req.body
    //B2: validate dữ liệu
    if(!mogoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    if(!body.username) {
        return res.status(400).json({
          message: 'Dữ liệu UserName không đúng'
        })
      }
      if(!body.firstname) {
        return res.status(400).json({
          message: 'Dữ liệu first Name không đúng'
        })
      }
      if(!body.lastname) {
        return res.status(400).json({
          message: 'Dữ liệu last Name không đúng'
        })
      }
      //B3: Gọi model thực hiện các thao tác nghiệp vụ
      // thu thập dữ liệu cập nhật
      let updateUser = {
        username: body.username,
        lastname: body.lastname,
        firstname: body.firstname
      }
      // cập nhật dữ liệu
      userModel.findByIdAndUpdate(id,updateUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Cập nhật dữ liệu thành công bằng  ID",
                 updateData: data
             })
         }
      })
};
//Tạo function deleteUserById
const deleteUserById = (req,res)=>{
    //B1: thu thập dữ liệu từ req
    let id= req.params.userId;
    //B2: validate dữ liệu
    if(!mogoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "Id không hợp lệ"
        })
    };
    //B3: Gọi model thực hiện các thao tác nghiệp vụ
    userModel.findByIdAndDelete(id,(err,data)=>{
        if(err){
            return res.status(500).json({
                 message: err.message
             })
         }
         else{
             return res.status(200).json({
                 message:"Thành công xoá dữ liệu thông qua ID",
                 DeleteData: data
             })
         }
    })
};

//exprot controller
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}

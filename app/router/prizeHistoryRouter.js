//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const prizeHistoryMiddeware = require("../middleware/middleware");
//tạo router
const prizeHistoryRouter = express.Router();

// improt controller
const prizeHistoryController = require("../Controller/prizeHistoryController");

prizeHistoryRouter.post("/prize-histories",prizeHistoryMiddeware.Middeware,prizeHistoryController.createPrizeHistory);
prizeHistoryRouter.get("/prize-histories",prizeHistoryMiddeware.Middeware,prizeHistoryController.getAllPrizeHistory);
prizeHistoryRouter.get("/prize-histories/:historyId",prizeHistoryMiddeware.Middeware,prizeHistoryController.getPrizeHistoryById);
prizeHistoryRouter.put("/prize-histories/:historyId",prizeHistoryMiddeware.Middeware,prizeHistoryController.updatePrizeHistoryById);
prizeHistoryRouter.delete("/prize-histories/:historyId",prizeHistoryMiddeware.Middeware,prizeHistoryController.deletePrizeHistoryById);



// exprot router
module.exports = {prizeHistoryRouter};


